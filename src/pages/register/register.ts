import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Profile } from '../../model/profile.interface';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from '@angular/fire/database';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  profile =  {} as Profile;
  password: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private angularAuth: AngularFireAuth, private db: AngularFireDatabase) {
  }
  
  async register() {
    try{
      const newUser = await this.angularAuth.auth.createUserWithEmailAndPassword(this.profile.email, this.password);
      this.profile.avatar = "assets/imgs/init.png";
      await this.db.object("Profile/"+newUser.user.uid).set(this.profile).
      then(() =>{this.navCtrl.pop();}).
      catch(reason=>alert(reason));
    } catch(reason) {
      alert(reason);
    }
  }
}
