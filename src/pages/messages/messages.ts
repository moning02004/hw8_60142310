import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { Observable, Subscription } from 'rxjs-compat';
import { map } from 'rxjs/operators';
import { Profile } from '../../model/profile.interface';
import { Message } from '../../model/message.interface';

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {
  // for key
  person: any;
  me: any;
  
  // message windows?
  msgList: Observable<any[]>;
  inputMsg: string;

  // others
  currentUser: Profile;
  message = {} as Message;
  
  readLast: Subscription;
  readMessage: Subscription;
  
  @ViewChild('content') ct: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams, private db: AngularFireDatabase, private auth:AngularFireAuth) {
    this.person =  this.navParams.get("person");
    this.me = this.auth.auth.currentUser.uid;
    console.log(this.me);
    this.db.object("Profile/"+this.me).valueChanges().subscribe((result:Profile) => {
      this.currentUser = result;
    });
  }

  ionViewDidLoad() {
    this.msgList = this.db.list(`Message-by-user/${this.me}/${this.person.key}`).valueChanges().pipe(map( changes => {
      changes.map( (mkey:{msg: string, fromID: string, msgkey: string, unread:Number}) => {
        this.db.object(`Message/${mkey.msgkey}`).valueChanges().subscribe( (x:{msg: string, fromID: string}) => {
          mkey.msg = x.msg; 
          mkey.fromID = x.fromID;
        });
      });
      return changes;
    }));

    this.readLast = this.db.list(`Message-by-user/${this.me}/${this.person.key}`).valueChanges().subscribe( (x:{length:Number}) => {
      if (x.length != 0) {
        this.db.list(`Last-message/${this.me}`).update(this.person.key, {unread: 0});
        if(this.ct) this.ct.scrollTo(0, 10000, 0);
      }
    });

    this.readMessage = this.db.list(`Message-by-user/${this.person.key}/${this.me}`).snapshotChanges().subscribe(x=>{
      x.forEach((xx, index)=> {
        let temp:any = x[x.length-1-index].payload.val();
        if (temp.unread == 0) return;
        this.db.list(`Message-by-user/${this.person.key}/${this.me}`).update(x[x.length-1-index].key, {unread: 0});
      });
    });
  }

  ionViewDidEnter() {
    if(this.ct) this.ct.scrollToBottom();
  }

  ionViewWillUnload() {
    this.readLast.unsubscribe();
    this.readMessage.unsubscribe();
  }
  
  async sendMessage(){
    this.message = {fromID: this.me, toID: this.person.key, msg: this.inputMsg};
    let key = await this.db.list('Message').push(this.message).key;

    
    this.db.database.ref(`Last-message/${this.person.key}/${this.me}`).transaction(function(msg) {
      return {lastmsgKey: key, unread: (msg ? msg.unread+1: 1)}; 
    });
    this.db.object(`Last-message/${this.me}/${this.person.key}`).set({lastmsgKey: key, unread: 0});

    await this.db.list(`Message-by-user/${this.me}/${this.person.key}`).push({msgkey: key, unread: 1});
    await this.db.list(`Message-by-user/${this.person.key}/${this.me}`).push({msgkey: key, unread: 0});
    
    this.inputMsg = "";
  }
}
