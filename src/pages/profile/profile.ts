import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { Profile } from '../../model/profile.interface';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoginPage } from '../login/login';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  uid: any;
  profile: Profile;
  subscription: any;
  editable: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private db:AngularFireDatabase, private auth: AngularFireAuth, private app: App) {
    this.uid = this.auth.auth.currentUser.uid;
    this.subscription = this.db.object("Profile/"+this.uid).valueChanges().subscribe((result:Profile) => {
      this.profile = result;
    });
  }

  async logout() {
    this.subscription.unsubscribe();
    await this.auth.auth.signOut();
    this.app.getRootNav().setRoot(LoginPage);
  }

  saveEdit() {
    console.log(this.editable);
  }
}
