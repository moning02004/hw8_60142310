import { Component } from '@angular/core';
import { ChatPage } from '../chat/chat';
import { FriendsPage } from '../friends/friends';
import { ProfilePage } from '../profile/profile';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  chat = ChatPage;
  friends = FriendsPage;
  profile = ProfilePage;

  constructor() {
  }
}
