import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { Observable } from 'rxjs-compat';
import { map } from 'rxjs/operators';
import { MessagesPage } from '../messages/messages';
/**
 * Generated class for the FriendsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage {

  friendList: Observable<any[]>;
  uid: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private db: AngularFireDatabase, private auth:AngularFireAuth) {
    this.uid = this.auth.auth.currentUser.uid;
    this.friendList = this.db.list("Profile").snapshotChanges().pipe(map(changes => 
      changes.map(c => ({ key: c.payload.key, ...c.payload.val()}))));
  }

  gotoMessage(person) {
    this.navCtrl.push(MessagesPage, {person: person});
  }
}
