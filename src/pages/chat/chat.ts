import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { MessagesPage } from '../messages/messages';
import { Profile } from '../../model/profile.interface';
import { Message } from '../../model/message.interface';

import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  me: any;
  chatList: Observable<any[]>;
  chat_key:string;
  chat_profile:Profile
  msg:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private db: AngularFireDatabase, private auth:AngularFireAuth) {
    this.me = this.auth.auth.currentUser.uid;
    
  }
  ionViewDidLoad() {
    this.chatList = this.db.list(`Last-message/${this.me}`).valueChanges().pipe(map( changes => {
      changes.map((msg:{lastmsgKey:string, peerID:string, profile:Profile, msg:string, unread: number}) => {
        this.db.object(`Message/${msg.lastmsgKey}`).valueChanges().subscribe((msgs:Message)=>{
          msg.peerID = (this.me != msgs.fromID)? msgs.fromID : msgs.toID;
          msg.msg = msgs.msg;
          this.db.object(`Profile/${msg.peerID}`).valueChanges().subscribe((result:Profile)=>{
            msg.profile = result;
          });
        });
      });
      return changes;
    }));
  }
  gotoMessage(chat) {
    let person = {
      key: chat.peerID,
      firstName: chat.profile.firstName,
      avatar: chat.profile.avatar
    };
    this.navCtrl.push(MessagesPage, {person: person});
  }
}
