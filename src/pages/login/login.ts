import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email: string;
  password: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AngularFireAuth) {
    this.email = this.password = "";
  }

  gotoRegister(){
    this.navCtrl.push(RegisterPage);
  }
  loginKeyup(event) {
    if (event.code == "Enter") {
      this.login();
    }
  }
  async login() {
    if (this.email != "" && this.password != "") {
      await this.auth.auth.signInWithEmailAndPassword(this.email, this.password).
      then((user)=>{
        this.navCtrl.setRoot(TabsPage);
      }).
      catch((reason)=>{
        alert(reason);
      });
    }
  }
}