export interface Profile{
    avatar: string;
    email: string,
    firstName: string,
    lastName: string,
    phone: string,
    status: string
}