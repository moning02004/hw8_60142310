export interface Message{
    fromID: string,
    toID: string,
    msg: string
}